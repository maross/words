Words

*Architettura*

L'app è stata sviluppata seguendo un design architetturale del tipo MVVM+Coordinator e sfrutta
le potenzialità della reactive programming (RxSwift-RxCocoa) soprattutto per il
bindig con la UI.

Le componenti principali sono:

- Model
- Services
- ViewModel
- View(Controller)
- Coordinator

Questo tipo di modularizzazione permette di avere un buon disaccoppiamento, i ViewController
(o Scena) vengono di fatto utilizzati esclusivamente nel loro ruolo originario di View e tutta
la logica di business viene incaplsulata all'interno di un ViewModel che ha una dipendenza diretta
con uno o più Services e un Coordinator (simile al Router che troviamo nel VIPER).
I Services sono gli unici componenti che interagiscono attivamente con i Model.

Il Coordinator ha lo scopo di astrarre la logica di routing all'interno dell'app e permette di
non avere riferimenti espliciti, ad esempio, all'interno dei ViewController che è agnostico rispetto
al flusso di navigazione.

L'app è composta da due scene a lista: Books e Words.

*Books*

La prima lista mostra i libri (file .txt) che sono inclusi all'interno della cartella Books, il BookService
si occupa di generare i Book scansionando la cartella e il ViewModel traduce il modello in altri
ViewModel (BookCellModel) questa volta relativi alle celle. Il setup della tableView è delegato al
framework RxDataSources.
In questo esempio non è presente uno strato di persistenza, ciò è dovuto al fatto
che ho preferito rendere evidente la fase di "searching" delle singole parole ad ogni selezione, chiaramente
è possibile eseguire questo processo in modo lazy e salvare il risultato del conteggio associandolo ad un'entità
apposita che rappresenti il dominio del Book.
Per ralizzare ciò si può ricorrere al paradigma Repository (protocol oriented), avendo due specializzazioni
ad esempio se volessimo utilizzare Core Data o Realm indistintamente.

*Words*

La seconda lista mostra i risultati della ricerca parole del testo, nella navigation bar è possibile filtrare
per vedere solo i risultati contenenti un numero primo di occorrenze, a destra invece è presente un bar button
per l'ordinamento (alfabetico/numerico).
La logica di ricerca e verifica del numero primo è rappresentata da due extension (String+Words e Int+Prime)
utilizzate dal WordCountService che si occupa della creazione dei WordCount.
L' intera operazione (apertura file e ricerca) viene eseguita in background e solo la prima volta.
Durante la fase di search viene mostrata un'apposita schermata di loading che indica in maniera più evidente che è
in atto il processing.

*Testing*

 Per ogni componente principale sono presenti gli unit test relativi.
