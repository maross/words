//

import UIKit

protocol TargetScene {
    var transition: SceneTransitionType { get }
}

enum Scene {
    case main(BooksViewModel)
    case words(WordsViewModel)
}

extension Scene: TargetScene {
    var transition: SceneTransitionType {
        switch self {
        case let .main(viewModel):
            var mainVC = BooksViewController.instantiate()
            mainVC.bind(to: viewModel)
            let rootVC = UINavigationController(rootViewController: mainVC)
            return .root(rootVC)
        case let .words(viewModel):
            var vc = WordsViewController.instantiate()
            vc.bind(to: viewModel)
            return .push(vc)
        }
    }
}
