//

import Foundation

public struct WordCount {
    public let word: String
    public let count: Int
    public let isPrimeNumber: Bool
    
    public init(word: String, count: Int) {
        self.word = word
        self.count = count
        self.isPrimeNumber = count.isPrime
    }
}

extension WordCount: Equatable {
    public static func == (lhs: WordCount, rhs: WordCount) -> Bool {
        return lhs.word == rhs.word &&
            lhs.count == rhs.count
    }
}
