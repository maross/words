//

import Foundation

public struct Book: Codable {
    public let title: String
    public let fileURL: URL
    
    init(fileURL: URL) {
        self.title  = fileURL.lastPathComponent
        self.fileURL = fileURL
    }
    
    public init(title: String, fileURL: URL) {
        self.title = title
        self.fileURL = fileURL
    }
}

extension Book: Equatable {
    public static func == (lhs: Book, rhs: Book) -> Bool {
        return lhs.title == rhs.title &&
            lhs.fileURL == rhs.fileURL
    }
}

