//

import Foundation
import RxSwift
import RxCocoa
import Action

extension OrderOptions {
    var string: String {
        switch self {
        case .wordAsc:
            return "🔤⬆️"
        case .wordDesc:
            return "🔤⬇️"
        case .countAsc:
            return "🔢⬆️"
        case .countDesc:
            return "🔢⬇️"
        }
    }
}

extension FilterOptions {
    var string: String {
        switch self {
        case .all:
            return "All 🅰️"
        case .prime:
            return "Prime 🅿️"
        }
    }
}


class WordsViewModel: ViewModelType {
    // MARK: Inputs & Outputs
    struct Input {

    }
    
    struct Output {
        var filterBy: Observable<FilterOptions>
        var orderBy: Observable<OrderOptions>
        var words: Driver<[SearchCellModel]>
        var error: Driver<Error>
        var executing: Driver<Bool>
    }
    
    lazy var showOnlyPrimeAction: Action<Bool, Void> = {
        Action<Bool, Void> { [unowned self] prime in
            self.filterByProperty.onNext(prime ? .prime : .all)
            return .empty()
        }
    }()
    
    lazy var orderByWordAction: Action<Bool, Void> = {
        Action<Bool, Void> {  [unowned self] ascending in
            self.orderByProperty.onNext(ascending ? .wordDesc : .countAsc)
            return .empty()
        }
    }()
    
    lazy var orderByCountAction: Action<Bool, Void> = {
        Action<Bool, Void> {  [unowned self] ascending in
            self.orderByProperty.onNext(ascending ? .countDesc : .wordAsc)
            return .empty()
        }
    }()
    
    // MARK: Private
    private let book: Book
    private let service: WordCountService
    private let sceneCoordinator: SceneCoordinatorType?
    private let refreshProperty = BehaviorSubject<Bool>(value: true)
    private let orderByProperty = BehaviorSubject<OrderOptions>(value: .wordAsc)
    private let filterByProperty = BehaviorSubject<FilterOptions>(value: .all)
    
    // MARK: Init
    init(book: Book, service: WordCountService = WordCountService(),
         sceneCoordinator: SceneCoordinatorType? = SceneCoordinator.shared) {
        self.book = book
        self.service = service
        self.sceneCoordinator = sceneCoordinator
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
    
        let refresh = refreshProperty.asObservable()
        let orderBy = orderByProperty.asObservable()
        let filterBy = filterByProperty.asObservable()
        let words = Observable
            .combineLatest(refresh, orderBy, filterBy)
            .flatMap { refresh, orderBy, filterBy -> Observable<[SearchCellModel]> in
                return self.service.countWords(of: self.book, orderBy: orderBy, filterBy: filterBy)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .map { $0.map { SearchCellModel(wordCount: $0) } }
        }
        
        return Output(filterBy: filterBy,
                      orderBy: orderBy,
                      words: words.asDriver(onErrorJustReturn: []),
                      error: errorTracker.asDriver(),
                      executing: activityIndicator.asDriver())
        
        
    }
}
