//

import UIKit
import RxSwift
import RxDataSources
import Reusable
import StatefulViewController

class WordsViewController: UIViewController, BindableType, StoryboardSceneBased {
    // MARK: StoryboardSceneBased
    static let sceneStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    // MARK: ViewModel
    var viewModel: WordsViewModel!
    
    // MARK: IBOutlets
    @IBOutlet var tableView: UITableView!
    
    // MARK: Private
    typealias WordsSectionModel = SectionModel<String, SearchCellModel>
    private var dataSource: RxTableViewSectionedReloadDataSource<WordsSectionModel>!
    private let disposeBag = DisposeBag()
    private var navBarButton: UIButton!
    private var rightBarButtonItem: UIBarButtonItem!
    private var hasData = false
    
    // MARK: Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup placeholder views
        loadingView = LoadingView()
        emptyView = EmptyView()
        errorView = ErrorView()
        self.setupInitialViewState()
        
        configureNavigationController()
        configureTableView()
    }
    
    // MARK: BindableType
    func bindViewModel() {
        let input = WordsViewModel.Input()
        let output = viewModel.transform(input: input)
        
        output.filterBy
            .subscribe { [unowned self] filterBy in
                guard let filterBy = filterBy.element else { return }
                switch filterBy {
                case .all:
                    self.navBarButton.rx.bind(to: self.viewModel.showOnlyPrimeAction, input: true)
                case .prime:
                    self.navBarButton.rx.bind(to: self.viewModel.showOnlyPrimeAction, input: false)
                }
            }
            .disposed(by: disposeBag)
        
        output.filterBy
            .map { $0.string }
            .bind(to: navBarButton.rx.title())
            .disposed(by: disposeBag)
        
        output.orderBy
            .subscribe { [unowned self] orderBy in
                guard let orderBy = orderBy.element else { return }
                switch orderBy {
                case .wordAsc:
                    self.rightBarButtonItem.rx.bind(to: self.viewModel.orderByWordAction, input: true)
                case .wordDesc:
                    self.rightBarButtonItem.rx.bind(to: self.viewModel.orderByWordAction, input: false)
                case .countAsc:
                    self.rightBarButtonItem.rx.bind(to: self.viewModel.orderByCountAction, input: true)
                case .countDesc:
                    self.rightBarButtonItem.rx.bind(to: self.viewModel.orderByCountAction, input: false)
                }
            }
            .disposed(by: disposeBag)
        
        output.orderBy
            .map { $0.string }
            .bind(to: rightBarButtonItem.rx.title)
            .disposed(by: disposeBag)
        
        output.words
            .do(onNext: { words in
                self.hasData = words.count > 0
                self.endLoading()
            })
            .map { [WordsSectionModel(model: "words", items: $0)] }
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        output.executing.drive(onNext: { executing in
            guard !self.hasContent() else { return }
            if executing {
                self.startLoading()
            }
        }).disposed(by: disposeBag)
        
        output.error.drive(onNext: { error in
            self.endLoading(error: error)
        }).disposed(by: disposeBag)
    }
    
    // MARK: UI
    private func configureNavigationController() {
        navBarButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        navBarButton.setTitleColor(.black, for: .normal)
        navBarButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        rightBarButtonItem = UIBarButtonItem()
        navigationItem.titleView = navBarButton
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationController?.navigationBar.tintColor = .black
    }
    
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 44
        tableView.register(cellType: SearchCell.self)
        dataSource = RxTableViewSectionedReloadDataSource<WordsSectionModel>(
            configureCell: tableViewDataSource
        )
    }
    
    private var tableViewDataSource: RxTableViewSectionedReloadDataSource<WordsSectionModel>.ConfigureCell {
        return { _, tableView, indexPath, cellModel in
            var cell: SearchCell = tableView.dequeueReusableCell(for: indexPath)
            cell.bind(to: cellModel)
            return cell
        }
    }
}


extension WordsViewController: StatefulViewController {
    func hasContent() -> Bool {
        return self.hasData
    }
    
    func handleErrorWhenContentAvailable(_ error: Error) {}
}
