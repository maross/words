//

import Foundation
import RxSwift
import RxCocoa
import Differentiator

class SearchCellModel: ViewModelType {
    
    // MARK: Inputs & Outputs
    struct Input {}
    struct Output {
        let word: Driver<String>
        let count: Driver<String>
        let isPrime: Driver<Bool>
    }
    
    // MARK: Private
    private let word: Driver<String>
    private let count: Driver<String>
    private let isPrime: Driver<Bool>
    
    let wordCount: WordCount
    // MARK: Init
    init(wordCount: WordCount) {
        self.wordCount = wordCount
        let wordCountStream = Driver.just(wordCount)
        self.word = wordCountStream.map { $0.word }
        self.count = wordCountStream.map { String($0.count) }
        self.isPrime = wordCountStream.map { $0.isPrimeNumber }
    }
    
    func transform(input: Input) -> Output {
        return Output(word: word, count: count, isPrime: isPrime)
    }
}

extension SearchCellModel: Equatable {
    public static func == (lhs: SearchCellModel, rhs: SearchCellModel) -> Bool {
        return lhs.wordCount.word == rhs.wordCount.word
    }
}

extension SearchCellModel: IdentifiableType {
    var identity: String {
        return self.wordCount.word
    }
}
