//

import UIKit
import RxSwift
import Reusable

class SearchCell: UITableViewCell, NibReusable, BindableType {
    // MARK: ViewModel
    var viewModel: SearchCellModel!
    
    // MARK: IBOutlets
    @IBOutlet var wordLabel: UILabel!
    @IBOutlet var countLabel: UILabel!
    
    // MARK: Private
    private var disposeBag = DisposeBag()
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    func bindViewModel() {
        let outputs = viewModel.transform(input: SearchCellModel.Input())
        
        outputs.word
            .drive(wordLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs.count
            .drive(countLabel.rx.text)
            .disposed(by: disposeBag)
        
        outputs.isPrime
            .asObservable()
            .subscribe(onNext: { [weak self] isPrime in
                self?.countLabel.textColor = isPrime ? UIColor.blue : UIColor.black
            })
            .disposed(by: disposeBag)
    }
}
