//

import UIKit
import RxSwift
import Reusable

class BookCell: UITableViewCell, NibReusable, BindableType {

    // MARK: ViewModel
    var viewModel: BookCellModel!

    // MARK: IBOutlets
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!

    // MARK: Private
    private var disposeBag = DisposeBag()

    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func prepareForReuse() {
        super.prepareForReuse()

        disposeBag = DisposeBag()
    }

    func bindViewModel() {
        let outputs = viewModel.transform(input: BookCellModel.Input())

        outputs.title
            .drive(titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
}
