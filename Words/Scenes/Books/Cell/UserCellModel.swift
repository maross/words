//

import Foundation
import RxSwift
import RxCocoa
import Differentiator

class BookCellModel: ViewModelType {

    // MARK: Inputs & Outputs
    struct Input {}
    struct Output {
        let title: Driver<String>
//        let coverImage: Driver<Image>
    }
    
     // MARK: Private
    private let title: Driver<String>

    let book: Book
    // MARK: Init
    init(book: Book) {
        self.book = book
        let bookStream = Driver.just(book)
        self.title = bookStream.map { $0.title }
    }
    
    func transform(input: Input) -> Output {
        return Output(title: title)
    }
}

extension BookCellModel: IdentifiableType {
    var identity: String {
        return self.book.title
    }
}


