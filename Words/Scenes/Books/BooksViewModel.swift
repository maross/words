//

import Foundation
import RxSwift
import RxCocoa

class BooksViewModel: ViewModelType {
    // MARK: Inputs & Outputs
    struct Input {
        var trigger: Driver<Void>
        var selection: Driver<IndexPath>
    }
    
    struct Output {
        var books: Driver<[BookCellModel]>
        var error: Driver<Error>
        var executing: Driver<Bool>
        var bookSelected: Driver<Book>
    }
    
    // MARK: Init
    let service: BookServiceType
    let sceneCoordinator: SceneCoordinatorType?
    
    init(service: BookServiceType = BookService(),
         sceneCoordinator: SceneCoordinatorType? = SceneCoordinator.shared) {
        self.service = service
        self.sceneCoordinator = sceneCoordinator
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        
        let books = input.trigger.flatMapLatest {
            return self.service.allBooks()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriver(onErrorJustReturn: [])
                .map { $0.map { BookCellModel(book: $0) } }
        }
        
        let bookSelected = input.selection.withLatestFrom(books) { (indexPath, books) -> Book in
            return books[indexPath.row].book
            }.do(onNext: { book in
                let viewModel = WordsViewModel(book: book)
                 self.sceneCoordinator?.transition(to: Scene.words(viewModel))
            })
        
        return Output(books: books,
                      error: errorTracker.asDriver(),
                      executing: activityIndicator.asDriver(),
                      bookSelected: bookSelected
        )
    }
}
