//

import UIKit
import RxSwift
import RxCocoa

import RxDataSources
import Reusable

class BooksViewController: UIViewController, BindableType, StoryboardSceneBased {
    
    // MARK: StoryboardSceneBased
    static let sceneStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    // MARK: ViewModel
    var viewModel: BooksViewModel!
    
    // MARK: IBOutlets
    @IBOutlet var tableView: UITableView!
    
    // MARK: Private
    typealias BooksSectionModel = SectionModel<String, BookCellModel>
    private var dataSource: RxTableViewSectionedReloadDataSource<BooksSectionModel>!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "📚 Books"
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 56
        tableView.register(cellType: BookCell.self)
        dataSource = RxTableViewSectionedReloadDataSource<BooksSectionModel>(
            configureCell: tableViewDataSource
        )
    }
    
    private var tableViewDataSource: RxTableViewSectionedReloadDataSource<BooksSectionModel>.ConfigureCell {
        return { _, tableView, indexPath, cellModel in
            var cell: BookCell = tableView.dequeueReusableCell(for: indexPath)
            cell.bind(to: cellModel)
            return cell
        }
    }
    
    // MARK: BindableType
    func bindViewModel() {
        let input = BooksViewModel.Input(trigger: Driver.just(()),
                                    selection: tableView.rx.itemSelected.asDriver())
        let output = viewModel.transform(input: input)
        
        output.books
            .asObservable()
            .map { [BooksSectionModel(model: "books", items: $0)] }
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        output.bookSelected
        .drive()
        .disposed(by: disposeBag)
    }

}
