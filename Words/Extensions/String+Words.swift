//

import Foundation

extension String {
    func wordOccurrencies(ignoreCase: Bool = true) -> [String: Int] {
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.punctuationCharacters)
        let components = self.components(separatedBy: chararacterSet)
        let words = components.filter { !$0.isEmpty }
        let grouped = Dictionary(grouping: words, by: { ignoreCase ? $0.lowercased() : $0 }).mapValues { items in items.count }
        return grouped
    }
}
