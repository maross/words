//

import Foundation

extension Int {
    /*
     This prime checking function and extension is the most efficient as it checks the divisibility of only
     sqrt(n)/2 integers.
     */
    var isPrime: Bool {
        guard self >= 2     else { return false }
        guard self != 2     else { return true  }
        guard self % 2 != 0 else { return false }
        return !stride(from: 3, through: Int(sqrt(Double(self))), by: 2).contains { self % $0 == 0 }
    }
}
