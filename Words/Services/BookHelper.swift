//

import Foundation

func getAllTxtFrom(bundle: Bundle = Bundle.main, folder: String) -> [URL] {
    guard let txtURLs = bundle.urls(forResourcesWithExtension: "txt", subdirectory: folder) else { return [] }
    return txtURLs
}
