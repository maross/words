//

import Foundation
import RxSwift

protocol BookServiceType {
    func allBooks() -> Observable<[Book]>
}
