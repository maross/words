//

import Foundation
import RxSwift

class WordCountService: WordCountServiceType {
    private var book: Book?
    private var wordCounts: [WordCount] = []
    
    private func doSearch(_ book: Book) -> [WordCount] {
        if self.book != book {
            self.book = book
            do {
                var encoding: String.Encoding = String.Encoding(rawValue: 0)
                let text = try String(contentsOf: book.fileURL, usedEncoding: &encoding)
                self.wordCounts = text.wordOccurrencies()
                    .map { key, value in WordCount(word: key, count: value) }
            } catch {
                return []
            }
        }
        return self.wordCounts
    }
    
    func countWords(of book: Book, orderBy: OrderOptions = .countDesc, filterBy: FilterOptions = .prime) -> Observable<[WordCount]> {
        return Observable.deferred {
            let counts = self.doSearch(book)
                .filter { word in
                    if case .prime = filterBy {
                        return word.isPrimeNumber
                    }
                    return true
                }
                .sorted {
                    switch orderBy {
                    case .wordAsc:
                        return $0.word < $1.word
                    case .wordDesc:
                        return $0.word > $1.word
                    case .countAsc:
                        return $0.count < $1.count
                    case .countDesc:
                        return $0.count > $1.count
                    }
            }
            return .just(counts)
        }.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }
}
