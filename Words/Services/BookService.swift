//

import Foundation
import RxSwift

class BookService: BookServiceType {
    func allBooks() -> Observable<[Book]> {
        let bookFiles = getAllTxtFrom(folder: "Books")
        return Observable.just(bookFiles).map { $0.map { Book.init(fileURL: $0)} }
    }
}
