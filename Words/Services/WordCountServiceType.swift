//

import Foundation
import RxSwift

enum OrderOptions {
    case wordAsc
    case wordDesc
    case countAsc
    case countDesc
}

enum FilterOptions {
    case all
    case prime
}

protocol WordCountServiceType {
    func countWords(of book: Book, orderBy: OrderOptions, filterBy: FilterOptions) -> Observable<[WordCount]>
}
