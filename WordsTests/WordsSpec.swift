//

import Quick
import Nimble
@testable
import Words

class WordsSpec: QuickSpec {
    override func spec() {
        describe("detection") {
            it("skips whitespaces - lower/uppercased") {
                let string = "A b C d E f G h I l"
                let words = string.wordOccurrencies()
                expect(words.count) == 10
            }
            
            it("skips newlines") {
                let string = """
                             Line1
                             Line2
                             Line3
                             """
                let words = string.wordOccurrencies()
                expect(words.count) == 3
            }
            
            it("skips punctuation") {
                let string = "!? One,Two Three, Four; Five. "
                let words = string.wordOccurrencies()
                expect(words.count) == 5
            }
        }
        
        describe("case") {
            let string = "test Test TEst TESt TEST"
            
            it("ignores case of same word") {
                let words = string.wordOccurrencies(ignoreCase: true)
                expect(words.count) == 1
                expect(words["test"]) == 5
            }
            
            it("NOT ignores case of same word") {
                let words = string.wordOccurrencies(ignoreCase: false)
                expect(words.count) == 5
                expect(words["test"]) == 1
            }
        }
    }
}
