//

import Quick
import Nimble
import RxBlocking
import RxSwift
import RxCocoa
import RxTest
@testable import Words

extension BookService {
    func testBooks() -> Observable<[Book]> {
        let bookFiles = TestHelper.testBooks()
        return Observable.just(bookFiles)
    }
}

class BookServiceSpec: QuickSpec {
    override func spec() {
        var service: BookService!
        var scheduler: TestScheduler!
        var disposeBag: DisposeBag!
        
        beforeEach {
            scheduler = TestScheduler(initialClock: 0)
            SharingScheduler.mock(scheduler: scheduler) {
                service = BookService()
            }
            disposeBag = DisposeBag()
        }
        
        afterEach {
            scheduler = nil
            service = nil
            disposeBag = nil
        }
        
        it("reads txt from folder") {
            let observer = scheduler.createObserver([Book].self)
            
            scheduler.scheduleAt(100) {
                service.testBooks().subscribe(observer).disposed(by: disposeBag)
            }
            
            scheduler.start()
            
            let results = observer.events.first
                .map { event in
                    event.value.element!.count
            }
            
            expect(results) == 5
        }
        
    }
    
}

