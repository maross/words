//

import Quick
import Nimble
import RxBlocking
import RxSwift
import RxCocoa
import RxTest
@testable import Words

class WordCountServiceSpec: QuickSpec {
    override func spec() {
        var service: WordCountService!
        var scheduler: TestScheduler!
        var disposeBag: DisposeBag!
        
        beforeEach {
            scheduler = TestScheduler(initialClock: 0)
            SharingScheduler.mock(scheduler: scheduler) {
                service = WordCountService()
            }
            disposeBag = DisposeBag()
        }
        
        afterEach {
            scheduler = nil
            service = nil
            disposeBag = nil
        }
        
        it("open book1.txt and count words") {
            let expectation1 = self.expectation(description: "Count words event")
            let book1 = TestHelper.book1()
            let observer = scheduler.createObserver([WordCount].self)
            let words = service.countWords(of: book1).share()
            
            words.subscribe(observer).disposed(by: disposeBag)
            
            words.subscribe(onNext: { (wordCount) in
                expectation1.fulfill()
            }).disposed(by: disposeBag)
            
            scheduler.start()
            
            self.waitForExpectations(timeout: 5, handler: nil)
            
            let results = observer.events.first
                .map { event in
                    event.value.element!.count
            }
            expect(results) == 1569
        }
    }
}
