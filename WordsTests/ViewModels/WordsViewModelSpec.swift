//

import Quick
import Nimble
import RxBlocking
import RxSwift
import RxTest
import RxCocoa
@testable import Words

class WordsViewModelSpec: QuickSpec {
    override func spec() {
        var viewModel: WordsViewModel!
        var scheduler: TestScheduler!
        var disposeBag: DisposeBag!
        let book2: Book = TestHelper.book2()
        
        beforeEach {
            scheduler = TestScheduler(initialClock: 0)
            SharingScheduler.mock(scheduler: scheduler) {
                viewModel = WordsViewModel(book: book2)
            }
            disposeBag = DisposeBag()
        }
        afterEach {
            scheduler = nil
            viewModel = nil
            disposeBag = nil
        }
        
        it("Show no words") {
            let input = WordsViewModel.Input()
            let output = viewModel.transform(input: input)
            
            let expectation = self.expectation(description: "Count words event")
            let observer = scheduler.createObserver([SearchCellModel].self)
            let words = output.words.asObservable().share()
            
            words.subscribe(observer).disposed(by: disposeBag)
            words.subscribe(onNext: { (wordCount) in
                expectation.fulfill()
            }).disposed(by: disposeBag)
            
            scheduler.start()
            
            self.waitForExpectations(timeout: 5, handler: nil)
            
            let results = observer.events.first
                .map { event in
                    event.value.element!.count
            }
            
            expect(results) == 0
        }
    }
}
