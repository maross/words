//

import Quick
import Nimble
import RxBlocking
import RxSwift
import RxTest
import RxCocoa
@testable import Words

class TestBookService: BookService {
    override func allBooks() -> Observable<[Book]> {
        let bookFiles = TestHelper.testBooks()
        return Observable.just(bookFiles)
    }
}

class BooksViewModelSpec: QuickSpec {
    override func spec() {
        var viewModel: BooksViewModel!
        var scheduler: TestScheduler!
        var disposeBag: DisposeBag!
        
        beforeEach {
            scheduler = TestScheduler(initialClock: 0)
            SharingScheduler.mock(scheduler: scheduler) {
                viewModel = BooksViewModel(service: TestBookService(), sceneCoordinator: nil)
            }
            disposeBag = DisposeBag()
        }
        afterEach {
            scheduler = nil
            viewModel = nil
            disposeBag = nil
        }
        
        it("Show all 5 test books") {
            let trigger = PublishSubject<Void>()
            let selection = PublishSubject<IndexPath>()
            let input = BooksViewModel.Input(trigger: trigger.asDriver(onErrorJustReturn: ()),
                                             selection: selection.asDriver(onErrorJustReturn: IndexPath(row: 0, section: 0)))
            let output = viewModel.transform(input: input)
            
            let observer = scheduler.createObserver([BookCellModel].self)
            
            scheduler.scheduleAt(100) {
                output.books.asObservable().subscribe(observer).disposed(by: disposeBag)
            }
            
            scheduler.scheduleAt(200) {
                trigger.onNext(())
            }
            
            scheduler.start()
            
            let results = observer.events.first
                .map { event in
                    event.value.element!.count
            }
            
            expect(results) == 5
        }
        
        it("Select first book (book1.txt)") {
            let selection = PublishSubject<IndexPath>()
            let input = BooksViewModel.Input(trigger: Driver.just(()),
                                             selection: selection.asDriver(onErrorJustReturn: IndexPath(row: 0, section: 0)))
            let output = viewModel.transform(input: input)
            
            let observer = scheduler.createObserver(Book.self)
            
            scheduler.scheduleAt(100) {
                output.bookSelected.asObservable().subscribe(observer).disposed(by: disposeBag)
            }
            
            scheduler.scheduleAt(200) {
                let first = IndexPath(row: 0, section: 0)
                selection.onNext(first)
            }
            
            scheduler.start()
            
            let results = observer.events.first
                .map { event in
                    event.value.element?.title
            }
            
            expect(results) == "book1.txt"
        }
    }
}
