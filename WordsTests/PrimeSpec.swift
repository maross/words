//

import Quick
import Nimble
@testable
import Words

class PrimeSpec: QuickSpec {
    
    private func generatePrimeNumbers(to n: Int) -> [Int] {
        var composite = Array(repeating: false, count: n + 1) // The sieve
        var primes: [Int] = []
        
        if n >= 150 {
            // Upper bound for the number of primes up to and including `n`,
            // from https://en.wikipedia.org/wiki/Prime_number_theorem#Non-asymptotic_bounds_on_the_prime-counting_function :
            let d = Double(n)
            let upperBound = Int(d / (log(d) - 4))
            primes.reserveCapacity(upperBound)
        } else {
            primes.reserveCapacity(n)
        }
        
        let squareRootN = Int(Double(n).squareRoot())
        var p = 2
        while p <= squareRootN {
            if !composite[p] {
                primes.append(p)
                for q in stride(from: p * p, through: n, by: p) {
                    composite[q] = true
                }
            }
            p += 1
        }
        while p <= n {
            if !composite[p] {
                primes.append(p)
            }
            p += 1
        }
        return primes
    }
    
    override func spec() {
        it("detects if a number is prime") {
            let primeNumbers = self.generatePrimeNumbers(to: 10000)
            let checks = primeNumbers.map{ $0.isPrime }
            expect(checks.allSatisfy({$0 == true})) == true
        }
    }
}
