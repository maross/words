//

import Foundation
import RxSwift
@testable
import Words

class TestHelper {
    static func book1() -> Book {
        return testBooks().first(where: { $0.title == "book1.txt" })!
    }
    
    static func book2() -> Book {
        return testBooks().first(where: { $0.title == "book2.txt" })!
    }
    
    static func testBooks() -> [Book] {
        let bundle = Bundle(for: TestHelper.self)
        let bookFiles = getAllTxtFrom(bundle: bundle, folder: "TestBooks")
        return bookFiles.map { Book(fileURL: $0) }.sorted { $0.title < $1.title }
    }
}


